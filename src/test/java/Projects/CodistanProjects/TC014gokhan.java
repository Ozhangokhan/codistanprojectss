package Projects.CodistanProjects;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC014gokhan {

	@Test
	public void tc014() {
		
		String str = "Hello World";
		
		String replace = str.replace('o', 'a');
		
		System.out.println(replace);
		
		Assert.assertEquals(replace, "Hella Warld");
	}
	
}
