package Projects.CodistanProjects;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC016gokhan {

	@Test
	public void tc016() {

		String str = "Hello World";

		String loCase = str.toLowerCase();

		System.out.println(loCase);

		String expected = "hello world";

		Assert.assertEquals(loCase, expected);
	}
}