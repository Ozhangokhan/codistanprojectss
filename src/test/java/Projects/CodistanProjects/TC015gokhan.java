package Projects.CodistanProjects;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC015gokhan {
	
	@Test
	public void tc015() {
		
		String str = "Hello World";
		
		String upCase = str.toUpperCase();
		
		System.out.println(upCase);
		
		String expected = "HELLO WORLD";
		
		Assert.assertEquals(upCase, expected);
	}
}
